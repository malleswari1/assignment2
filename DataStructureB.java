package com.greatLearning.assignment2;

import java.util.*;

public class DataStructureB{

	public void cityNameCount(ArrayList<Employee> employees) {
	
		// TODO Auto-generated method stub
		ArrayList<String> City=new ArrayList<String>();
		City.add(employees.get(0).getCity());
		City.add(employees.get(1).getCity());
		City.add(employees.get(2).getCity());
		City.add(employees.get(3).getCity());
		City.add(employees.get(4).getCity());
		
		HashSet<String> hs=new HashSet<String>(City);
		
	
		TreeMap<String, Integer> hmap=new TreeMap<String,Integer>();
		 for(String  strele:hs) {
			 
			 hmap.put(strele, Collections.frequency(City, strele));
			 
			}
		System.out.println("Count of Employees from each city:");
			 
		System.out.println(""+hmap);
	
		
	}

}
