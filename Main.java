package com.greatLearning.assignment2;
import java.util.*;
public class Main {
	public static void main(String[] args) {
		ArrayList<Employee> employees = new ArrayList<>();
		Employee e1=new Employee();
		Employee e2=new Employee();
		Employee e3=new Employee();
		Employee e4=new Employee();
		Employee e5=new Employee();
		
		e1.Employeedetails(1,"Aman",20 ,1100000, "IT", "Delhi");
		e2.Employeedetails(2,"Bobby", 22 ,500000, "HR", "Bombay");
		e3.Employeedetails(3 ,"Zoe", 20, 750000 ,"Admin" ,"Delhi");
		e4.Employeedetails(4 ,"Smitha", 21 ,1000000," IT", "Chennai");
		e5.Employeedetails(5, "Smitha" ,24, 1200000, "HR"," Bengaluru");
		employees.add(e1);
		employees.add(e2);
		employees.add(e3);
		employees.add(e4);
		employees.add(e5);
		System.out.println("display employee details");
		e1.getEmployeeInfo();
		e2.getEmployeeInfo();
		e3.getEmployeeInfo();
		e4.getEmployeeInfo();
		e5.getEmployeeInfo();
		System.out.println(".......................");
		DataStructureA name=new DataStructureA();
		name.sortingNames(employees);
		System.out.println(".......................");
		
		DataStructureB City=new DataStructureB();
		City.cityNameCount(employees);
		System.out.println(".......................");
		
		Salary sal=new Salary();
		sal.monthlySalary(employees);
		//e1.setId(1);
		}
}
