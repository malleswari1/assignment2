package com.greatLearning.assignment2;

import java.util.*;

public class Employee {
int id;
String name;
int age;
int salary; // per annum
String department;
String city;

public void Employeedetails(int id,String name,int age,int salary,String department,String city) {
	try {
		this.id=id;
		if(id<0) {
			throw new IllegalArgumentException("Employee Id should not be less than or equal to zero");
			
		}
		this.name=name;
		if(name.isEmpty()||name==null) {
			throw new IllegalArgumentException("Employee name should not be empty or null ");
			
		}
		this.age=age;
		if(age < 0) {
			throw new IllegalArgumentException("Employee age should not be less than or equal to zero");
			
		}
		
		this.salary=salary;
		if(salary < 0) {
			throw new IllegalArgumentException("Employee salary should not be less than or equal to zero");
			
		}
		
		this.department=department;
		if(department.isEmpty()||department==null) {
			throw new IllegalArgumentException("Employee salary should not be empty or null");
			
		}

		this.city=city;
		if(city.isEmpty()||city==null) {
			throw new IllegalArgumentException("Employee city should not be empty or null");
			
		}
		
	}
	catch(Exception e) {
		System.out.println(""+e.getMessage());
	}
}
		

public int getId() {
	return id;
}
public void setId(int id) {
	this.id=id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name=name;
}


public int getAge() {
	return age;
}
public void setage(int age) {
	this.age=age;
}

public double getSalary() {
	return salary;
}
public void setSalary(int salary) {
	this.salary=salary;
}

public String getDepartment() {
	return department;
}
public void setDepartment(String department) {
	this.department=department;
}

public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city=city;
}

public void getEmployeeInfo() {  
    System.out.println(" "+id+" "+name+" "+age+" "+salary+" "+" "+department+" "+city); 
}
}

